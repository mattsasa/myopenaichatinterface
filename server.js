import OpenAI from "openai"
import { createServer } from "http"
import { WebSocketServer } from "ws"
import express from "express"

const openai = new OpenAI()

const port = process.env.PORT
const app = express()
const server = createServer(app)
const wss = new WebSocketServer({ server })

wss.on("connection", async (socket) => {
    // Set up a 20-second ping interval
    const connectionHeartbeat = setInterval(() => {
        socket.ping()
    }, 20000)

    socket.on("close", () => {
        clearInterval(connectionHeartbeat)
    })

    const conversationHistory = []
    conversationHistory.push({ role: "system", content: "You are a helpful assistant." })

    socket.on("message", async (msgString) => {
        let msg
        try {
            msg = JSON.parse(msgString)
        } catch (error) {
            console.log("Failed to parse JSON message")
            return
        }

        if (msg.restoreHistory) {
            // Restore State from a reconnect
            conversationHistory.push(...msg.conversationHistory)
            socket.send(JSON.stringify({ stateRestored: true }))
            return
        }

        // Regular User Msg
        conversationHistory.push(msg)

        const inProgressHeartbeat = setInterval(() => {
            socket.send(JSON.stringify({ heartbeat: true }))
        }, 3000)

        try {
            const completion = await openai.chat.completions.create({
                messages: conversationHistory,
                model: "gpt-4o",
            })
            socket.send(JSON.stringify(completion.choices[0].message))
            conversationHistory.push(completion.choices[0].message)
        } catch (error) {
            console.log(error)
        } finally {
            // Ensure that the interval is always cleared
            clearInterval(inProgressHeartbeat)
        }
    })
})

app.use(express.static("html"))

server.listen(port, () => {
    console.log(`App listening on port ${port}`)
})
