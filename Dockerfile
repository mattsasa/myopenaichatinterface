# Use an official Node runtime as a parent image
FROM node:19

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package.json ./

# Install any needed packages
RUN npm install

# Bundle app source
COPY . .

# Run the app when the container launches
CMD ["npm", "run", "serve"]
